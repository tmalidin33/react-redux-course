const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpackBundleAnalyzer = require("webpack-bundle-analyzer");

process.env.NODE_ENV = "production";

module.exports = {
	entry: path.join(__dirname, "src", "index.js"),
	target: "web",
	devtool: "source-map",
	output: {
		path: path.join(__dirname, "build"),
		publicPath: "/",
		filename: "bundle.js",
	},
	mode: "production",
	resolve: { modules: [path.resolve(__dirname, "src"), "node_modules"] },
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ["babel-loader"],
			},
			{
				test: /\.(css|scss)$/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: "css-loader",
						options: {
							sourceMap: true,
						},
					},
					{
						loader: "postcss-loader",
						options: {
							postcssOptions: {
								plugins: () => [require("cssnano")],
							},
							sourceMap: true,
						},
					},
				],
			},
			{
				test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
				use: ["file-loader"],
			},
		],
	},
	plugins: [
		new webpackBundleAnalyzer.BundleAnalyzerPlugin({
			analyzerMode: "static",
		}),
		new MiniCssExtractPlugin({
			filename: "[name].[contenthash].css",
		}),
		new webpack.DefinePlugin({
			"process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
			"process.env.API_URL": JSON.stringify("http://localhost:3001"),
		}),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, "src", "index.html"),
			favicon: "src/favicon.ico",
			minify: {
				removeComments: true,
				collapseWhitespace: true,
				removeRedundantAttributes: true,
				useShortDoctype: true,
				removeEmptyAttributes: true,
				removeStyleLinkTypeAttributes: true,
				keepClosingSlash: true,
				minifyJS: true,
				minifyCSS: true,
				minifyURLs: true,
			},
		}),
		new ESLintPlugin({
			// Plugin options
			extensions: ["js", "jsx"],
			formatter: require.resolve("react-dev-utils/eslintFormatter"),
			eslintPath: require.resolve("eslint"),
			failOnError: false,
			emitWarning: true,
			// ESLint class options
			resolvePluginsRelativeTo: __dirname,
		}),
	],
};
