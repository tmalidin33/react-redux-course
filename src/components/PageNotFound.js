import React from "react";

const PageNotFound = () => {
	return <h1>Oups! Page not Found.</h1>;
};
export default PageNotFound;
