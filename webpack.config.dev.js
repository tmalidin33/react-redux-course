const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

process.env.NODE_ENV = "development";

module.exports = {
	entry: path.join(__dirname, "src", "index.js"),
	target: "web",
	devtool: "cheap-module-source-map",
	output: {
		path: path.join(__dirname, "build"),
		publicPath: "/",
		filename: "bundle.js",
	},
	mode: process.env.NODE_ENV || "development",
	resolve: { modules: [path.resolve(__dirname, "src"), "node_modules"] },
	// devServer: { contentBase: path.join(__dirname, "src") },
	devServer: {
		stats: "minimal",
		overlay: true,
		historyApiFallback: true,
		disableHostCheck: true,
		headers: { "Access-Control-Allow-Origin": "*" },
		https: false,
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ["babel-loader"],
			},
			{
				test: /\.(css|scss)$/,
				use: ["style-loader", "css-loader"],
			},
			{
				test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
				use: ["file-loader"],
			},
		],
	},
	plugins: [
		new webpack.DefinePlugin({
			"process.env.API_URL": JSON.stringify("http://localhost:3001"),
		}),
		new HtmlWebpackPlugin({
			template: path.join(__dirname, "src", "index.html"),
			favicon: "src/favicon.ico",
		}),
		new ESLintPlugin({
			// Plugin options
			extensions: ["js", "jsx"],
			formatter: require.resolve("react-dev-utils/eslintFormatter"),
			eslintPath: require.resolve("eslint"),
			failOnError: false,
			emitWarning: true,
			// ESLint class options
			resolvePluginsRelativeTo: __dirname,
		}),
	],
};
